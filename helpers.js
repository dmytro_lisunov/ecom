'use strict';

const fs = require('fs');
const path = require('path');
const spawn = require('child_process').spawn;
const spawnSync = require('child_process').spawnSync;
const webDavCLIPath = path.resolve(__dirname, 'node_modules', 'davos-cli', 'index.js');

function getSiteName (brandCode = fetchBrandCode()) {
	let brands = {
		"as": "aspesi",
		"br": "baracuta",
		"bd" : "bdbaggies",
		"bi" : "biffi",
		"ca" : "casadei",
		"ch" : "charlotte",
		"co" : "coccinelle",
		"dg" : "dolcegabbana",
		"dj" : "ladoublej",
		"lncc" : "lncc2",
		"rl" : "rucoline",
		"sc" : "scarosso",
		"sp" : "spiewak",
		"wp" : "wallpaper",
		"sw" : "weitzman",
		"vw" : "westwood"
	}
	return brands[brandCode.toLocaleLowerCase()];
}

function getDirectories(srcpath) {
    return fs.readdirSync(srcpath).filter(function (file) {
        return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
}

function fetchBrandCode(cwd = process.cwd()) {
	let pathChunksArr = require('path').basename(cwd).split('ecom-');
	if (pathChunksArr.length === 2) {
		return pathChunksArr[pathChunksArr.length - 1];
	}
	throw new Error('Wrong start path. It should be a folder like [ecom-as], [ecom-vw], etc.');
}

function fetchCartridgeName(options) {
	let { isMobile, cwd, pattern } = Object.assign({}, {
		isMobile: false,
		cwd:process.cwd()
	}, options);
	if (!pattern) {
		let brandCode = fetchBrandCode(cwd);
		let patternStr = isMobile ?
			brandCode + '_mobile' :
			(brandCode === 'lncc' ? 'lncc_redesign' : brandCode);
		pattern = new RegExp(patternStr + '$');
	}
	return getDirectories(path.resolve(cwd, 'cartridges'))
		.filter( function (cartridge) {
			return cartridge !== 'node_modules' &&
				!cartridge.match(/^\./) &&
				cartridge.match(pattern)
		})[0];
}

function fetchAllCartridgeNames(cwd = process.cwd()) {
	return getDirectories(path.resolve(cwd, 'cartridges'))
		.filter( (cartridge) => cartridge !== 'node_modules' && !cartridge.match(/^\./) );
}

function parseCLIOptions(str) {
	return str
		.split(new RegExp('^cli\\s','gi'))[1]
		.split(new RegExp('\\s+','gi'));
}

function executeWebDavConfig(webdavConfig = {}, dir = process.cwd()) {
	let config = webdavConfig.slice(0);
	config[0].config.cartridge = fetchAllCartridgeNames(dir);
	fs.writeFileSync(path.resolve(dir, 'davos.json'), JSON.stringify(config, null, 4), 'utf8');
}

function uploadProject(webdavConfig, dir) {
	console.log('\x1b[33m',process.cwd());

	const currentDir = process.cwd();
	process.chdir(dir);

	executeWebDavConfig(webdavConfig, dir);

	let davos = spawn( 'node', [webDavCLIPath, 'upload:cartridges', '--verbose']);
	davos.stdout.on('data', (data) => console.log('\x1b[33m',`\n[WebDav]: ${data}`));
	davos.stderr.on('data', (data) => console.error('\x1b[31m',`[WebDav] ERROR: ${data}`));
	davos.on('close', (code) => {
		if (code !== 0) { console.error('\x1b[31m',`[WebDav] ERROR: davOS process has exited with code ${code}`); }
		fs.unlinkSync(path.resolve(process.cwd(), 'davos.json'))
		process.chdir(currentDir);
		console.log('\x1b[33m',process.cwd());
	});
	return davos;
}

function uploadAndWatch (webdavConfig, options = [], cwd = process.cwd()) {
	const currentDir = process.cwd();
	let config = webdavConfig.slice(0);
	config[0].config.cartridge = fetchAllCartridgeNames(cwd);

	process.chdir('./cartridges');
	console.log('\x1b[33m',process.cwd());
	fs.writeFileSync(path.resolve(process.cwd(), 'davos.json'), JSON.stringify(config, null, 4), 'utf8');

	let davos = spawn( 'node', [webDavCLIPath, 'watch'].concat(options), { shell: true, stdio: 'inherit' } );
	davos.on('close', (code) => {
		if (code !== 0) { console.error('\x1b[31m',`[WebDav] davOS process has exited with code ${code}`); }
		process.chdir(currentDir);
		console.log('\x1b[33m',process.cwd());
	});
	return davos;
}

let helpers = {
	getDirectories: getDirectories,
	fetchBrandCode: fetchBrandCode,
	fetchCartridgeName: fetchCartridgeName,
	fetchAllCartridgeNames: fetchAllCartridgeNames,
	getSiteName: getSiteName,
	parseCLIOptions: parseCLIOptions,
	uploadProject: uploadProject,
	uploadAndWatch: uploadAndWatch
};

module.exports = helpers;
