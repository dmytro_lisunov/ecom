# ecom

Ecom is all-in-one building and uploading tool for TLG Projects on Demandware (SCC) platform.
It is based on:

* WebPack
* WebDav (davOS)
* Chokidar
* BrowserSync
* Dart-Sass: https://sass-lang.com/dart-sass
* etc.

And it works like Fiddler, UX Studio and Gulp.

## Instalation

`npm install --global https://bitbucket.org/dmytro_lisunov/ecom.git`

## Usage

go to project root folder e.g. `D:\TLG\repo\ecom-rl` (where `.git` folder is located) and type in Terminal:

`ecom`

or

`ecom [options]`. For examble, use mobile cartridge and mobile Storefront: `ecom --mobile` or simply `ecom -m`;

For `DEVELOPMENT` instance view type `ecom --dev`.

For enable files uploading into Sandbox add `dw.json` file into your folder structure like this:

```
D:\
  |
  - TLG
      |
     repo
        |
        - dw.json (locate it here)
        - ecom-oneapp
        - ecom-vw
        - ecom-rl (start "ecom" here: D:\TLG\repo\ecom-rl)
            |
            - .git/
            - .gitignore
            - cartridges/
```

Options:

* `-v, --version                  ` -  output the version number
* `--clean                        ` -  re-upload OneApp cartridges and Current project cartridges
* `-w, --watch                    ` -  watch files. Default is <true>. Use --no-watch to disable
* `--no-watch                     ` -  Disable file watching
* `-m, --mobile                   ` -  mobile cartridge and mobile Storefront view should be used
* `-s, --serve                    ` -  Run localhost proxy server (by default). Use --no-serve for canceling
* `--no-serve                     ` -  Disable localhost proxy server
* `--dev                          ` -  use Storefront of `DEVELOPMENT` instance (uploading will be disabled automatically)
* `--stage                        ` -  use Storefront of `STAGING` instance (uploading will be disabled automatically)
* `--domain <url>                 ` -  use custom Storefront URL Domain, e.g. for <PRODUCTION> instance (default: hostname of your Sandbox from `dw.json`, e.g. dev21-store-thelevelgroup.demandware.net)
* `--https                        ` -  use https for Storefront URL (instead of `http`)
* `--country <countryCode>        ` -  Set country in Country Selector (default: `IT`)
* `--lang <lang>                  ` -  Set language in Language selector (e.g. `en`, `de`, etc. Default value: `default`)
* `--host <hostname>              ` -  Hostname for uploading cartridges to Sandbox via WebDav. Default is using from <dw.json> file (default: hostname of your Sandbox from `dw.json`, e.g. dev21-store-thelevelgroup.demandware.net)
* `--upload                       ` -  Enable cartridges uploading via WebDav. Use `--no-upload` to disable. Default is `true`
* `--no-upload                    ` -  Disable cartridge uploading via WebDav
* `--webpack [webpack-cli options]` -  Add additional Webpack CLI Options. Example: $ ecom --webpack "cli --mode --config"     More info: https://webpack.js.org/api/cli/
* `--webdav [WebDav options]      ` -  Add additional WebDav Client Options. Example: $ ecom --webdav "cli --user user1 --config"       More info: https://www.npmjs.com/package/davos-cli
* `-h, --help                     ` -  output usage information

## Known issues

### Chrome browser

In case of errors like `TOO MANY REDIRECTS` or `SSL_UNTRUSTED_PROTOCOL`, etc. just:

1. Clean up Coockies and Local Storage
2. **Mandatory**: Clear Chrome browsing data:  `Settings -> Clear browsing data -> ☑ Cached images and files `

### Ruby Sass (ruby-sass)

In case of stack trace like this:

```
STDERR Unable to load the EventMachine C extension; To use the pure-ruby reactor, require 'em/pure_ruby'
sass.js:69
Unable to load the EventMachine C extension; To use the pure-ruby reactor, require 'em/pure_ruby'
C:/Ruby24/lib/ruby/site_ruby/2.4.0/rubygems/core_ext/kernel_require.rb:59:in `require': cannot load such file -- 2.4/rubyeventmachine (LoadError)
    from C:/Ruby24/lib/ruby/site_ruby/2.4.0/rubygems/core_ext/kernel_require.rb:59:in `require'
    from C:/Ruby24/lib/ruby/gems/2.4.0/gems/eventmachine-1.2.7-x86-mingw32/lib/rubyeventmachine.rb:2:in `<top (required)>'
    ...
    from D:/TLG/repo/ecom/node_modules/ruby-sass/server.rb:3:in `<main>'

```

do next:

1. go to this folder `C:\Ruby24-x64\lib\ruby\gems\2.4.0\gems\eventmachine-1.2.5-x64-mingw32\lib`
2. open this file `eventmachine.rb`
3. write this: `require 'em/pure_ruby'` in the first line of code in the file

This will make it work with no issues. More info https://github.com/tabler/tabler/issues/155
