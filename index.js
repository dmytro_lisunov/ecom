'use strict';

const path = require('path');
const fs = require('fs');
const spawn = require('child_process').spawn;
let lodashStr = require('lodash/string');
const cli = require('commander');

const packageFile = require('./package');
const ecomHelpers = require('./helpers');

module.exports = function () {};

let cwd = process.cwd();
let dwJson;
let dwJsonPath = path.resolve(cwd, '../dw.json');
try {
	dwJson= require(dwJsonPath);
} catch (e) {
	console.error('\x1b[31m', `[WebDav]: config file in path ${dwJsonPath} does not exist!`);
	console.error('\x1b[31m', `[WebDav]: Uploading is disabled`);
	console.error('\x1b[0m');
}
let hostname = dwJson ? dwJson.hostname : '';

cli
    .version(packageFile.version, '-v, --version')
    .option('--clean', 're-upload OneApp cartridges and Current project cartridges')
    .option('-w, --watch', 'watch files. Default is <true>. Use --no-watch to disable')
    .option('--no-watch', 'Disable file watching')
    .option('-m, --mobile', 'mobile cartridge and mobile Storefront view should be used')
    .option('-s, --serve', 'Run localhost proxy server (by default). Use --no-serve for canceling')
    .option('--no-serve', 'Disable localhost proxy server')
    .option('--dev', 'use Storefront of <DEVELOPMENT> instance')
    .option('--stage', 'use Storefront of <STAGING> instance')
    .option('--domain <url>', 'use custom Storefront URL Domain, e.g. for <PRODUCTION> instance', hostname)
    .option('--https', 'use https for Storefront URL (instead of http)')
    .option('--country <countryCode>', 'Set country in Country Selector', 'IT')
    .option('--lang <lang>', 'Set language in Language selector', 'default')
    //.option('-b, --build [build]', 'Build resources. It can be [sass], [js]', 'sass')
    .option('--host <hostname>', 'Hostname for uploading cartridges to Sandbox via WebDav. Default is using from <dw.json> file', hostname)
    .option('--upload', 'Enable cartridges uploading via WebDav. Use --no-upload to disable. Default is <true>')
    .option('--no-upload', 'Disable cartridge uploading via WebDav')
    .option('--webpack [webpack-cli options]', 'Add additional Webpack CLI Options. Example: $ ecom --webpack "cli --mode --config" \tMore info: https://webpack.js.org/api/cli/')
    .option('--webdav [WebDav options]', 'Add additional WebDav Client Options. Example: $ ecom --webdav "cli --user user1 --config" \t More info: https://www.npmjs.com/package/davos-cli')
    .parse(process.argv);

if (cli.dev || !cli.domain) { cli.domain = 'development-store-thelevelgroup.demandware.net'}
if (cli.stage) { cli.domain = 'staging-store-thelevelgroup.demandware.net'}

if (cli.watch === undefined) { cli.watch = true; };
if (cli.serve === undefined) { cli.serve = true; };
cli.upload = (!!hostname && (cli.upload === undefined || cli.upload === true)) ? true : false;
cli.country = cli.country.toUpperCase();

/////////////////////// Webpack config /////////////////////////////
let globalData = `
let ecomLib = '${lodashStr.escapeRegExp(path.resolve(__dirname))}';
let data = {};
data.watch = ${cli.watch};
data.serve = ${cli.serve};
data.mobile = ${cli.mobile};
data.domain = '${cli.domain}';
data.lang = '${cli.lang}';
data.country = '${cli.country}';
data.https = '${cli.https}';

`;

function build (env, argv) {
	const path = require('path');
	let ecomHelpers = require(path.resolve(ecomLib, 'helpers'));
	let ecomLibPath = path.resolve(ecomLib, 'node_modules');
	const extractTextWebpackPlugin = require(path.resolve(ecomLibPath, 'extract-text-webpack-plugin'));
	const browserSyncWebpackPlugin = require(path.resolve(ecomLibPath, 'browser-sync-webpack-plugin'));
	const fibers = require(path.resolve(ecomLibPath, 'fibers'));

	data.site = ecomHelpers.getSiteName();

	function customizeConf (config) {
		config.staticPath = path.resolve(process.cwd(), `cartridges/${config.cartridgeName}/cartridge/static/default/`);
		let customizedConfig = {
			name: config.name,
			watch: config.watch,
			devtool: 'source-map',
			mode: 'none',
			output: {
				path: path.resolve(config.staticPath, 'sasstarget'),
				filename: '[name].css'
			},
			resolveLoader: {
				modules: [ ecomLibPath ]
			},
			entry: config.entries.reduce( (obj, prop) => { obj[prop]=path.resolve(config.staticPath, 'sass', prop + '.scss'); return obj; }, {} ),
			module: {
				rules: [{
					test: /\.scss$/,
					use: extractTextWebpackPlugin.extract({
						use: [{
							loader: 'css-loader',
							options: {
								url: false,
								minimize: true,
								sourceMap: true
							}
						}, {
							loader: 'sass-loader',
							options: {
								sourceMap: true,
								implementation: require(path.resolve(ecomLibPath, 'dart-sass')),
								fiber: fibers,
								includePaths: [
									path.resolve(config.staticPath, 'sass'),
									path.resolve(`../ecom-oneapp/cartridges/${config.cartridgeOAName}/cartridge/static/default/sass`)
								]
							}
						}]
					})
				}]
			},
			plugins: [ new extractTextWebpackPlugin({ filename: '[name].css' }) ]
		};

		if (config.serve) {
			customizedConfig.plugins.push(new browserSyncWebpackPlugin({
				https: config.https ? true : false,
				port: 3000,
				proxy: {
					target: `http${config.https ? 's' : ''}://${config.domain}/on/demandware.store/Sites-${config.site}-Site/${config.lang}/Home-SwitchDeviceType?to=${config.mobile ? 'mobile': 'desktop'}`,
					proxyReq: [
						function(proxyReq) {
							proxyReq.setHeader('Cookie', `preferredLanguage=${config.lang};preferredCountry=${config.country}`);
							if (config.mobile) { proxyReq.setHeader('User-Agent', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1')}
						}
					]
				},
				rewriteRules: config.entries.map( (entry) => {
					return {
						match: new RegExp('href=.*' + entry + '\.css', 'i'),
						fn: function(req, res, match) {
							return 'href="/sasstarget/' + entry + '.css"';
						}
					}
				}),
				serveStatic: [path.resolve(config.staticPath)]
			}, {
				name: config.site,
				injectCss: true,
				callback: function(req, BrowserSync) {
					let browserSync = BrowserSync.publicInstance;
					//browserSync.notify('Hello', 55000);
				}
			}));
		}

		return customizedConfig;
	}


	let resultConf =  [
		customizeConf(
			Object.assign({},
				data,
				{
					name: 'desktop',
					entries: [ 'style', 'fonts'],
					cartridgeName: ecomHelpers.fetchCartridgeName(),
					cartridgeOAName: 'app_one_tlg',
					watch: !!(data.watch && !data.mobile),
					serve: !!(data.serve && !data.mobile)
				}
			)
		),
		customizeConf(
			Object.assign({},
				data,
				{
					name: 'mobile',
					entries: [ 'style_mobile', 'fonts_mobile'],
					cartridgeName: ecomHelpers.fetchCartridgeName({isMobile: true}),
					cartridgeOAName: 'app_one_mobile',
					watch: !!(data.watch && data.mobile),
					serve: !!(data.serve && data.mobile)
				},
			)
		)
	];
	return resultConf;
}

if (!cli.clean) {
	let webpackConfigText = `'use strict';\n${globalData}\nmodule.exports = ${String(build)}\n`;
	const webpackConfigPath = path.resolve(__dirname, './webpack.config.js');
	fs.writeFileSync(webpackConfigPath, webpackConfigText, 'utf8');
	let webpackCLIOptions = cli.webpack ? ecomHelpers.parseCLIOptions(cli.webpack) : [];
	let webpackCLI = spawn(
		'node',
		[
			path.resolve(__dirname, 'node_modules', 'webpack-cli', 'bin', 'cli.js'),
			`--config "${webpackConfigPath}"`,
			`--config-name ${cli.mobile ? 'mobile' : 'desktop'}`,
			'--colors',
			'--progress'
		].concat(webpackCLIOptions), {
			shell: true,
			stdio: 'inherit'
		}
	);
}

///////////////////////// Uploading
// console.log(cli); process.exit(1);
// console.log(require('util').inspect(cli, { depth: 99 }));

if (cli.upload) {
	let webdavConfig = [{
		["active"]: true,
		["profile"]: ecomHelpers.fetchBrandCode(),
		["config"]: {
			"hostname": dwJson.hostname,
			"username": dwJson.username,
			"password": dwJson.password,
			"codeVersion": dwJson["code-version"],
			"exclude": [
				"**/node_modules/**",
				"**/.sass-cache/**"
			]
		}
	}];

	if (cli.clean) {
		ecomHelpers.uploadProject(webdavConfig, cwd);
		ecomHelpers.uploadProject(webdavConfig, '../ecom-oneapp');
	} else if (cli.watch) {
		let webdavOptions = cli.webdav ? ecomHelpers.parseCLIOptions(cli.webdav) : [];
		ecomHelpers
			.uploadProject(webdavConfig, cwd)
			.on('close', (code) => {
				ecomHelpers.uploadAndWatch(webdavConfig, webdavOptions);
			});
	}
}
